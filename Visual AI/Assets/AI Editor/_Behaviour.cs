﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using Tree.Editor;

[System.Serializable]
public abstract class _Behaviour : ScriptableObject {

    [HideInInspector]
    public GameObject agentObj; //the attached agent game object
    [HideInInspector]
    public NavMeshAgent navigation; //the attached agents nav mesh agent
    public NodeData node; //the behaviours node
    [HideInInspector]
    public AIController controller; //the agents controller 

    public bool gameSaveLoaded = false;

    public enum BehaviourResult
    {
        SUCCESS = 0,
        FAILURE = 1,
        RUNNING = 2
    }
    // Use this for initialization
    public AITree tree;

    /// <summary>
    /// Called Upon Application Start Up.
    /// </summary>
    public abstract void Start();

    /// <summary>
    /// Called before entering the behaviour execute function.
    /// </summary>
    public abstract void OnEnter();


    public BehaviourResult result = BehaviourResult.FAILURE; //attach to the end of every return if you wish to see its result in the editor tool.

    /// <summary>
    /// Called Every Frame. Do Main Behaviour Here.
    /// </summary>
    public abstract BehaviourResult execute(); //execute behaviour

    /// <summary>
    /// Returns The Weight Value Of The Executed Behaviour. (Based On Whats Been Provided).
    /// </summary>
    public abstract float CalculateWeight(); //calc weight score
}
