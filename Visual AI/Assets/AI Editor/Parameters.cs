﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public struct ParameterData
{
    public string paramName;
    public int iParamValue;
    public float fParamValue;
    public bool bParamValue;
}




namespace Tree.Editor
{
    [Serializable]
    public enum ParameterType
    {
        BOOL = 0,
        INT = 1,
        FLOAT = 2,
    }
    [Serializable]
    public enum NumericalConditionType
    {
        GREATER = 0,
        LESS = 1,
        EQUAL = 2,
    }
    [Serializable]
    public enum BooleanConditionType
    {
        TRUE = 0,
        FALSE = 1,
    }

    [Serializable]
    public class Parameters 
    {
        public ParameterType type; //Parameter data type.
        public NumericalConditionType nConditionTypes; //parameter condition type for numerical types.
        public BooleanConditionType bConditionTypes;  //parameter condition type for bools.
        
        //The values set in the parameters window at the top of the inspector (i.e. if the value is set at 35 and requires the continous value to be greater for the condition to be met).
        public float fConditionvalue = 0;
        public int iConditionvalue = 0;
        public bool bConditionvalue = false;
        ///////////////////////////////////////////////

        //the values that user changes on the node.
        public float fContinuousValue = 0;
        public int iContinuousValue = 0;
        public bool bContinuousValue = false;

        public string paramName; //parameter name

        public bool selected = false; //if the parameter is selected in the inspector window.

        [HideInInspector]
        public ParameterData pData;

        public void OnCreate(string createdType) //called when creating a new parameter
        {
            type = (ParameterType)Enum.Parse(typeof(ParameterType), createdType); //set the type
            paramName = "Untitled"; //set default name .
        }

        /// <summary>
        /// Testing if the parameter conditions are met.
        /// </summary>
        public bool Test()
        {
            if (type == ParameterType.INT)
            {
                if (nConditionTypes == NumericalConditionType.GREATER)
                {
                    if (iContinuousValue > iConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.LESS)
                {
                    if (iContinuousValue < iConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.EQUAL)
                {
                    if (iContinuousValue == iConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            if (type == ParameterType.FLOAT)
            {
                if (nConditionTypes == NumericalConditionType.GREATER)
                {
                    if (fContinuousValue > fConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.LESS)
                {
                    if (fContinuousValue < fConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (nConditionTypes == NumericalConditionType.EQUAL)
                {
                    if (fContinuousValue == fConditionvalue)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            if (type == ParameterType.BOOL)
            {
                if (bConditionTypes == BooleanConditionType.TRUE)
                {
                    if (bContinuousValue == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                if (bConditionTypes == BooleanConditionType.FALSE)
                {
                    if (bContinuousValue == false)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
            }
            return true;
        }
    }
}




