﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;

namespace Tree.Editor
{
    public class ParameterUtilities
    {
       
        public static void DrawButtons(TreeEditorWindow editorWindow)
        {
            GUILayout.BeginVertical();
            GUILayout.Label("Create Parameter:");
            if (GUI.Button(new Rect(10, 35, 50, 20), "int"))
            {
                editorWindow.tree.CreateParameter("INT");
            }
            if (GUI.Button(new Rect(10, 60, 50, 20), "float"))
            {
                editorWindow.tree.CreateParameter("FLOAT");
            }
            if (GUI.Button(new Rect(10, 85, 50, 20), "bool"))
            {
                editorWindow.tree.CreateParameter("BOOL");
            }
            if (GUI.Button(new Rect(10,110,50,20), "Delete"))
            {
                RemoveParameter(editorWindow.tree);
                editorWindow.Repaint();
            }

            if (GUI.Button(new Rect(10, 135, 50, 20), "Clear"))
            {
                editorWindow.tree.ClearParameters();
                editorWindow.intParamScrollfieldY = 0;
                editorWindow.floatParamScrollfieldY = 0;
                editorWindow.boolParamScrollfieldY = 0;

                foreach (var node in editorWindow.tree.nodes)
                {
                    node.localParams.Clear();
                    node.localParamsDisplay.Clear();
                    node.localParamsDisplayOffset = 80;
                    node.selectedParameter.Clear();
                }

            }


            GUILayout.EndVertical();
        }
        /// <summary>
        /// This function is responsible for drawing the three labels above the global parameters fields.
        /// </summary>
        public static void DrawLabels(Vector2 namePos, Vector2 valuePos, Vector2 typePos)
        {
            GUI.Label(new Rect(namePos, new Vector2(100,20)), "Name:");
            GUI.Label(new Rect(valuePos, new Vector2(100, 20)), "Condition Value:");
            GUI.Label(new Rect(typePos, new Vector2(100, 20)), "Type:");
        }

        public static void RemoveParameter(AITree tree)
        {
            GUI.FocusControl(null);
            int i = 0;
            while (i < tree.globalParameters.Count)
            {
                if (tree.globalParameters[i].selected)
                {
                    tree.globalParameters.Remove(tree.globalParameters[i]);
                    i = 0;
                }
                else
                {
                    i++;
                }
            }
        }

        /// <summary>
        /// Draw parameter fields
        /// </summary>
        public static void DrawFields(Rect container, Rect nameDisplay, Rect valueDisplay, Rect typeDisplay, Parameters parameter)
        {
            Event e = Event.current;
            if (container.Contains(e.mousePosition) && !nameDisplay.Contains(e.mousePosition)
                && !valueDisplay.Contains(e.mousePosition) && !typeDisplay.Contains(e.mousePosition))
            {
                if (e.button == 0 && e.type == EventType.MouseDown)
                {
                    GUI.FocusControl(null);
                    if (!parameter.selected)
                    {
                        parameter.selected = true;
                    }
                    else
                    {
                        parameter.selected = false;
                    }
                }
            }

            if (parameter.selected)
            {
                GUI.color = Color.gray;
            }
            else
            {
                GUI.color = Color.white;
            }

            GUI.Box(container, "");
            GUI.color = Color.white;
            parameter.paramName = EditorGUI.TextField(nameDisplay, parameter.paramName);
            if (parameter.type == ParameterType.INT)
            {
                parameter.iConditionvalue = EditorGUI.IntField(valueDisplay, parameter.iConditionvalue);
                parameter.nConditionTypes = (NumericalConditionType)EditorGUI.EnumPopup(typeDisplay, parameter.nConditionTypes);
            }
            else if (parameter.type == ParameterType.FLOAT)
            {
                parameter.fConditionvalue = EditorGUI.FloatField(valueDisplay, parameter.fConditionvalue);
                parameter.nConditionTypes = (NumericalConditionType)EditorGUI.EnumPopup(typeDisplay, parameter.nConditionTypes);
            }
            else if (parameter.type == ParameterType.BOOL)
            {
                parameter.bConditionvalue = EditorGUI.Toggle(valueDisplay, parameter.bConditionvalue);
                parameter.bConditionTypes = (BooleanConditionType)EditorGUI.EnumPopup(typeDisplay, parameter.bConditionTypes);
            }
        }


        /// <summary>
        /// Responsible for drawing the global int parameter windows
        /// </summary>
        public static void DrawIntParametersWindow(TreeEditorWindow editorWindow)
        {
            Event e = Event.current;
            GUI.Label(new Rect(125 + 100, 15, 100, 20), "Int Parameters");
            Rect viewRect = new Rect(125, 35, 300, 150);

            GUI.Box(viewRect, "");
            int fieldYPos = 55;

            List<Parameters> globalIntParam = new List<Parameters>();
            foreach(var i in editorWindow.tree.globalParameters)
            {
                if (i.type == ParameterType.INT)
                {
                    globalIntParam.Add(i);
                }
            }

            if (globalIntParam.Count >= 5)
            {
                if (editorWindow.intParamScrollfieldY == 0)
                {
                    editorWindow.intParamScrollfieldY = viewRect.height;
                }
                Rect scrollRect = new Rect(viewRect.x, viewRect.y, viewRect.width, editorWindow.intParamScrollfieldY);
                editorWindow.iParameterScrollPosition = GUI.BeginScrollView(new Rect(125, 35, 300, 150), editorWindow.iParameterScrollPosition, scrollRect);
            }

            if (globalIntParam.Count > 0)
            {
                DrawLabels(new Vector2(135, 35), new Vector2(205, 35), new Vector2(305, 35));
            }

            for (int i = 0; i < globalIntParam.Count; i++)
            {
                DrawFields(new Rect(viewRect.x, fieldYPos - 4.5f, viewRect.width, 25), new Rect(135, fieldYPos, 100, 18),
                    new Rect(250, fieldYPos, 50, 18), new Rect(250 + 55, fieldYPos, 100, 20), globalIntParam[i]);


                fieldYPos += 25;
                
            }

            editorWindow.intParamScrollfieldY = fieldYPos;

            if (globalIntParam.Count >= 5)
            {
                GUI.EndScrollView();
            }
        }
        /// <summary>
        /// Responsible for drawing the global float parameter windows
        /// </summary>
        public static void DrawFloatParametersWindow(TreeEditorWindow editorWindow)
        {
            GUI.Label(new Rect(445 + 100, 15, 100, 20), "Float Parameters");
            Rect viewRect = new Rect(445, 35, 300, 150);
            GUI.Box(viewRect, "");
            int fieldYPos = 55;

            List<Parameters> globalFloatParam = new List<Parameters>();
            foreach (var i in editorWindow.tree.globalParameters)
            {
                if (i.type == ParameterType.FLOAT)
                {
                    globalFloatParam.Add(i);
                }
            }

            if (globalFloatParam.Count >= 5)
            {
                if (editorWindow.floatParamScrollfieldY == 0)
                {
                    editorWindow.floatParamScrollfieldY = viewRect.height;
                }
                Rect scrollRect = new Rect(viewRect.x, viewRect.y, viewRect.width, editorWindow.floatParamScrollfieldY);
                editorWindow.fParameterScrollPosition = GUI.BeginScrollView(new Rect(445, 35, 300, 150), editorWindow.fParameterScrollPosition, scrollRect);
            }

            if (globalFloatParam.Count > 0)
            {
                DrawLabels(new Vector2(455, 35), new Vector2(525, 35), new Vector2(625, 35));
            }
            for (int i = 0; i < globalFloatParam.Count; i++)
            {
                DrawFields(new Rect(viewRect.x, fieldYPos - 4.5f, viewRect.width, 25), new Rect(455, fieldYPos, 100, 18),
                    new Rect(570, fieldYPos, 50, 18), new Rect(625, fieldYPos, 100, 20), globalFloatParam[i]);
                fieldYPos += 25;
            }

            editorWindow.floatParamScrollfieldY = fieldYPos;

            if (globalFloatParam.Count >= 5)
            {
                GUI.EndScrollView();
            }
        }
        /// <summary>
        /// Responsible for drawing the global bool parameter windows
        /// </summary>
        public static void DrawBoolParametersWindow(TreeEditorWindow editorWindow)
        {
            GUI.Label(new Rect(445 + 320 + 100, 15, 100, 20), "Bool Parameters");
            Rect viewRect = new Rect(445 + 320, 35, 300, 150);
            GUI.Box(viewRect, "");
            int fieldYPos = 55;

            List<Parameters> globalBoolParam = new List<Parameters>();
            foreach (var i in editorWindow.tree.globalParameters)
            {
                if (i.type == ParameterType.BOOL)
                {
                    globalBoolParam.Add(i);
                }
            }

            if (globalBoolParam.Count >= 5)
            {
                if (editorWindow.boolParamScrollfieldY == 0)
                {
                    editorWindow.boolParamScrollfieldY = viewRect.height;
                }
                Rect scrollRect = new Rect(viewRect.x, viewRect.y, viewRect.width, editorWindow.boolParamScrollfieldY);
                editorWindow.bParameterScrollPosition = GUI.BeginScrollView(new Rect(445 + 320, 35, 300, 150), editorWindow.bParameterScrollPosition, scrollRect);
            }

            if (globalBoolParam.Count > 0)
            {
                DrawLabels(new Vector2(775, 35), new Vector2(845, 35), new Vector2(945, 35));
            }

            for (int i = 0; i < globalBoolParam.Count; i++)
            {
                DrawFields(new Rect(viewRect.x, fieldYPos - 4.5f, viewRect.width, 25), new Rect(775, fieldYPos, 100, 18),
                    new Rect(890, fieldYPos, 10, 18), new Rect(945, fieldYPos, 100, 20), globalBoolParam[i]);
                fieldYPos += 25;
            }

            editorWindow.boolParamScrollfieldY = fieldYPos;

            if (globalBoolParam.Count >= 5)
            {
                GUI.EndScrollView();
            }
        }

        public static void AddParameterToNode(NodeData node)
        {
            node.localParamsDisplay.Add(new Rect(10, node.localParamsDisplayOffset, 280, 25));
            node.localParamsDisplayOffset += 24;
            node.localParams.Add(new Parameters());
        }

        public static void RemoveParameterFromNode(NodeData node)
        {
            node.localParams.Remove(node.currentSelectedParam);
            int paramPos = node.localParamsDisplay.IndexOf(node.currentSelectedParamDisplay);
            node.localParamsDisplay.Remove(node.currentSelectedParamDisplay);
            RepoisionLocalUI(node, paramPos);
            node.localParamsDisplayOffset -= 24;
            node.selectedParameter.RemoveAt(paramPos);
        }

        public static void RepoisionLocalUI(NodeData node, int startIndex)
        {
            if (node.localParams.Count > 0)
            {
                node.currentSelectedParam = node.localParams[startIndex];
            }

            for (int i = startIndex; i < node.localParamsDisplay.Count; i++)
            {
                Rect newRect = node.localParamsDisplay[i];
                node.localParamsDisplay[i] = new Rect(newRect.x, newRect.y - 24, newRect.width, newRect.height);
            }
        }
    }
}




#endif
