﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
namespace Tree.Editor
{
    public class NodeUtilities
    {
        public static void EditorTreeExists(TreeEditorWindow editorWindow)
        {
            if (editorWindow.tree == null)
            {
                editorWindow.tree = ScriptableObject.CreateInstance<AITree>();
            }
        }

        public static void OnAddRootNode(TreeEditorWindow editorWindow)
        {
            EditorTreeExists(editorWindow);
            NodeData node = ScriptableObject.CreateInstance<NodeData>(); //create scriptable object instance
            node.nodeType = NodeType.Root; //set its type
                                           //Event e = Event.current;
            node.CreateNode(editorWindow.nodeID, 2, editorWindow.socketID, editorWindow); //create node, set its ids for sockets and node, set the current editor window 
            if (AssetDatabase.Contains(editorWindow.tree)) //if the tree has been saved, create the asset, without this the tree would add a missing identifier object to m_nodeData.
            {
                AssetDatabase.AddObjectToAsset(node, editorWindow.tree);
            }
           // editorWindow.m_nodeData.Add(node); //add it to nodes list
            editorWindow.tree.nodes.Add(node);
            editorWindow.socketID += 2; //increase socketId
            editorWindow.nodeID++; //Increase NodeId

        }
        public static void OnAddConditionNode(TreeEditorWindow editorWindow)
        {
            EditorTreeExists(editorWindow);
            NodeData node = ScriptableObject.CreateInstance<NodeData>();//create scriptable object instance
            node.nodeType = NodeType.Condition;//set its type
            node.CreateNode(editorWindow.nodeID, 2, editorWindow.socketID, editorWindow);//create node, set its ids for sockets and node, set the current editor window 
            if (AssetDatabase.Contains(editorWindow.tree))//if the tree has been saved, create the asset, without this the tree would add a missing identifier object to m_nodeData.
            {
                AssetDatabase.AddObjectToAsset(node, editorWindow.tree);
            }
            //editorWindow.m_nodeData.Add(node);//add it to nodes list
            editorWindow.tree.nodes.Add(node);
            editorWindow.socketID += 2; //increase socketId
            editorWindow.nodeID++;//Increase NodeId
        }
        public static void OnAddActionNode(TreeEditorWindow editorWindow)
        {
            EditorTreeExists(editorWindow);
            NodeData node = ScriptableObject.CreateInstance<NodeData>();//create scriptable object instance
            node.nodeType = NodeType.Action;//set its type
            node.CreateNode(editorWindow.nodeID, 2, editorWindow.socketID, editorWindow);//create node, set its ids for sockets and node, set the current editor window 
            if (AssetDatabase.Contains(editorWindow.tree))//if the tree has been saved, create the asset, without this the tree would add a missing identifier object to m_nodeData.
            {
                AssetDatabase.AddObjectToAsset(node, editorWindow.tree);
            }
            //editorWindow.m_nodeData.Add(node);//add it to nodes list
            editorWindow.tree.nodes.Add(node);
            editorWindow.socketID += 2;//increase socketId
            editorWindow.nodeID++;//Increase NodeId
        }
        public static void OnAddSelectorNode(TreeEditorWindow editorWindow)
        {
            EditorTreeExists(editorWindow);
            NodeData node = ScriptableObject.CreateInstance<NodeData>();//create scriptable object instance
            node.nodeType = NodeType.Selector;//set its type
            node.CreateNode(editorWindow.nodeID, 2, editorWindow.socketID, editorWindow);//create node, set its ids for sockets and node, set the current editor window 
            node.behaviour = BehaviourMenus.CreateBehaviour("Selector", editorWindow.tree);
            if (AssetDatabase.Contains(editorWindow.tree))//if the tree has been saved, create the asset, without this the tree would add a missing identifier object to m_nodeData.
            {
                AssetDatabase.AddObjectToAsset(node, editorWindow.tree);
            }
            // editorWindow.m_nodeData.Add(node);//add it to nodes list
            editorWindow.tree.nodes.Add(node);
            editorWindow.socketID += 2;//increase socketId
            editorWindow.nodeID++;//Increase NodeId
        }

        //public static void OnAddBlackBoardNode(TreeEditorWindow editorWindow)
        //{
        //    EditorTreeExists(editorWindow);
        //    NodeData node = ScriptableObject.CreateInstance<NodeData>();//create scriptable object instance
        //    node.nodeType = NodeType.BlackBoard;//set its type
        //    node.CreateNode(editorWindow.nodeID, 2, editorWindow.socketID, editorWindow);//create node, set its ids for sockets and node, set the current editor window 
        //    if (AssetDatabase.Contains(editorWindow.tree))//if the tree has been saved, create the asset, without this the tree would add a missing identifier object to m_nodeData.
        //    {
        //        AssetDatabase.AddObjectToAsset(node, editorWindow.tree);
        //    }
        //    // editorWindow.m_nodeData.Add(node);//add it to nodes list
        //    editorWindow.tree.nodes.Add(node);
        //    editorWindow.socketID += 2;//increase socketId
        //    editorWindow.nodeID++;//Increase NodeId
        //}


    }
}


#endif