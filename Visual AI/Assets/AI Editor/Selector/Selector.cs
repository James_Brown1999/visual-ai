﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Tree.Editor;


[CreateAssetMenu(fileName = "Selector", menuName = "AI Behaviour/Selector", order = 0)]
public class Selector : _Behaviour
{
    public List<Action> actions = new List<Action>(); //List of connected actions
    //public Action chosenAction = null;

    public enum SelectionMethod //choice
    {
        Roulette,
        BestChoice
    }

    public SelectionMethod selection;
    public Action chosenAction; //action selected
    private int chosenActionIter = 0;
    private bool actionSelected = false; //has an action been selected?

    public override void Start()
    {

    }

    public override void OnEnter()
    {

    }

    public override BehaviourResult execute()
    {
        if (actions.Count > 0) //if the selector is connected to any actions.
        {
            if (chosenAction == null)
            {
                tree.SetBool(false, "Chosen Action");
                actionSelected = false;
                if (Evaluate(agentObj) == BehaviourResult.SUCCESS) //if selectors evaluation returns success, begin running action
                {
                    return result = BehaviourResult.SUCCESS;
                }
                else
                {
                    return result = BehaviourResult.RUNNING; //continue searching for behaviour.
                }
            }
            if (chosenAction != null) //if an action has been chosen.
            {
                actionSelected = true;
                if (chosenAction.result == BehaviourResult.SUCCESS) //if action has completed successfully
                {
                    tree.SetBool(false, "Chosen Action");
                    //leave behaviour.
                    chosenAction.result = BehaviourResult.FAILURE;
                    chosenAction = null;
                    return result = BehaviourResult.RUNNING;
                }
                chosenAction.execute(); //execute chosen action by the selector      
                return result = BehaviourResult.SUCCESS;
            }
        }
        return result = BehaviourResult.FAILURE;
    }


    public BehaviourResult Evaluate(GameObject agentObj)
    {
        if (selection == SelectionMethod.BestChoice)
        {
            return BestChoiceEvaluation(agentObj);
        }
        
        if (selection == SelectionMethod.Roulette)
        {
            return RouletteEvaluation(agentObj);
        }

        return result = BehaviourResult.FAILURE;
    }

    public BehaviourResult BestChoiceEvaluation(GameObject agentObj)
    {
        List<float> scores = GetScores(agentObj, actions);

        //sort score
        scores.Sort(); //sort lowest scores lowest to highest

        foreach (var action in actions)
        {
            if (action.weightValue == scores[0]) //if the actions equal the best value
            {
                if (TestChosenAction(action)) //meets parameter conditions.
                {
                    return result = BehaviourResult.SUCCESS;
                }
                else
                {
                    return result = BehaviourResult.FAILURE; //leave
                }
            }
        }
        return result = BehaviourResult.FAILURE;
    }

    public BehaviourResult RouletteEvaluation(GameObject agentObj)
    {
        float sum = CalculateSum(GetScores(agentObj, actions)); //the sum of all the scores
        float probabilitySum = CalculateProbability(sum); //the sum of all the probability

        float dice = Random.Range(0, sum); //roll dice
        sum = 0; //reset sum to zero
        for (int i = 0; i < actions.Count; i++)
        {
            sum += actions[i].probability; //add actions prob
            if (sum > dice) //if its greater than the dice value
            {
                if (TestChosenAction(actions[i])) //if it meets the parameter conditions
                {
                    return result = BehaviourResult.SUCCESS; //begin running behaviour
                }
                else
                {
                    return result = BehaviourResult.FAILURE; //leave evaluation
                }
            }
        }
        return BehaviourResult.FAILURE;
    }


    bool TestChosenAction(Action action)
    {
        if (action.node.TestParameters()) //test all the parameters. Return success if all return true.
        {
            chosenAction = action;

            return true;
        }
        action = null;
        return false;
    }


    List<float> GetScores(GameObject agentObj, List<Action> actions)
    {
        List<float> scores = new List<float>();
        foreach (var action in actions)
        {
            scores.Add(action.CalculateWeight()); //Calculate weighted values of each node           
        }
        return scores;
    }

    float CalculateProbability(float sum)
    {
        float probSum = 0;

        //calculate prob
        foreach (var action in actions)
        {
            action.probability = probSum + (action.weightValue / sum);
            probSum += action.probability;
        }

        return probSum;
    }

    float CalculateSum(List<float> scores)
    {
        float sum = 0;
        //calculate sum 
        foreach (var action in actions)
        {
            sum += action.weightValue;
        }
        return sum;
    }

    public override float CalculateWeight()
    {
        return 0;
    }
}
