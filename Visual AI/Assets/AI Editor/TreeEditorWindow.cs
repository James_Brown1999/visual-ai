﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
//We only want to editor to exist in the editor, not the build.
#if UNITY_EDITOR
using UnityEditor;

namespace Tree.Editor
{
    public class TreeEditorWindow : EditorWindow, IHasCustomMenu
    {
        [HideInInspector]
        public int nodeID = 0; //Nodes unique ID
        [HideInInspector]
        public int socketID = 0; //sockets unique ID
        private Rect inspectorWindow; //the rect transform of the inspector window. (Found to the right of the screen)
        private Rect parameterWindow; //the rect transform of the parameter window. (Found at the top of the screen)
        public string inspectorName = ""; //Gets changed based on the current actions being performed (Will be node inspector if ..
        // node is selected)
        public NodeData currentSelectedNode;
        static bool startConnection = false; //determines whether to draw the connection curve to the mouse or to the connect node.
        private Rect saveButton; //save button transform
        public string treeName = ""; //behaviour trees name
        public Rect treeNameDisplay; //text box transform
        public Event e;
        public AITree tree; //the current tree being edited
        public GameObject selectedAgent; //the selected agent in the hierachy
        private bool playMode = false;
        public Vector2 iParameterScrollPosition = new Vector2(0, 0); //int param scroll position
        public Vector2 fParameterScrollPosition = new Vector2(0, 0); //float param scroll position
        public Vector2 bParameterScrollPosition = new Vector2(0, 0); //bool param scroll position
        ////////////The scroll length of each parameter window////////////
        public float intParamScrollfieldY = 0;
        public float floatParamScrollfieldY = 0;
        public float boolParamScrollfieldY = 0;

        // private Rect window;
        public void AddItemsToMenu(GenericMenu menu)
        {         
            //if no nodes are in the scene, only allow root nodes to be created.
            if (tree == null || tree.nodes.Count == 0)
            {
                menu.AddItem(new GUIContent("Add Behaviour Node/Root"), false, AddRoot);
            }
            else //only allow selectors, conditions and action nodes to created after root is added
            {
                menu.AddItem(new GUIContent("Add Behaviour Node/Condition"), false, AddCondition);
                menu.AddItem(new GUIContent("Add Behaviour Node/Action"), false, AddAction);
                menu.AddItem(new GUIContent("Add Behaviour Node/Selector"), false, AddSelector);
            }
            menu.ShowAsContext(); //display in context menu
            
        }
        
        //Add created node to the tree
        void AddRoot()
        {
            NodeUtilities.OnAddRootNode(this);
        }
        void AddCondition()
        {
            NodeUtilities.OnAddConditionNode(this);
        }
        void AddAction()
        {
            NodeUtilities.OnAddActionNode(this);
        }
        void AddSelector()
        {
            NodeUtilities.OnAddSelectorNode(this);
        }
        //////////////////////////////////////////////
        [MenuItem("Window/AI/Behaviour Editor")]
        public static TreeEditorWindow OpenTreeEditorWindow()
        {
            return GetWindow<TreeEditorWindow>("Behaviour Editor");
        }

        [UnityEditor.Callbacks.OnOpenAsset(1)]
        public static bool OnOpenData(int instanceID, int line)
        {
            //set the data of the tree to what the user has opened in inspector, and attempt to cast it as an AITree type.
            AITree data = EditorUtility.InstanceIDToObject(instanceID) as AITree;    

            //if data is set correctly (An actual tree asset is opened in inspector)
            if (data != null)
            {
                TreeEditorWindow editorWindow = OpenTreeEditorWindow(); //open the window

                editorWindow.LoadTree(data, editorWindow); //load the tree in the current window
                return true;
            }           
            return false;
        }
        void LoadTree(AITree data, TreeEditorWindow window = null)
        {
            if (window == null)
            {
                window = OpenTreeEditorWindow();
            }

            window.tree = data; //set the data
            window.treeName = data.name;
            window.nodeID = data.nodes[data.nodes.Count - 1].id + 1;
            window.socketID = data.nodes[data.nodes.Count - 1].sockets[1].id + 1;
            //////////////////////////////////////////////////////////////////

            //set the references for previous nodes in socket data. This is required for the drawing of curves.
            foreach (var node in tree.nodes)
            {
                node.editorWindow = window;
                foreach (var sock in node.sockets)
                {
                    if (sock.type == SocketType.Input && sock.previousNode != null)
                    {
                        sock.previousSocket = sock.previousNode.sockets[1];
                    }
                }
            }
        }

        void OnDestroy()
        {
            //close any open connections to curves that were selected. ()
            foreach (var node in tree.nodes)
            {
                if (node.connectionSelected)
                {
                    node.connectionSelected = false;
                }
            }
        }


        private void OnGUI()
        {        

            //Get the current UI event
            e = Event.current;
            //if user has right clicked and isnt in the process of connecting a node
           
            //draw background grid
            TreeEditorUtilities.DrawGrid(20, 0.2f, Color.gray, this);
            TreeEditorUtilities.DrawGrid(100, 0.9f, Color.gray, this);

            int id = 0;

            if (EditorApplication.isPlaying) //if an AI object with a tree is selected in the hierachy
            {
                playMode = true;
                if (Selection.activeGameObject != null) //if the editor is in playmode
                {
                    if (Selection.activeGameObject != selectedAgent) //if a new gameobject is selected.
                    {
                        //check if the selected object is a prefab instance, if so get reference to it so the original tree can be loaded back up.
                        if (PrefabUtility.GetCorrespondingObjectFromSource(Selection.activeGameObject) != null)
                        {
                            selectedAgent = (GameObject)PrefabUtility.GetCorrespondingObjectFromOriginalSource(Selection.activeObject);
                        }
                        else
                        {
                            selectedAgent = Selection.activeGameObject; //no prefab, just grab a reference to it.
                        }

                        if (Selection.activeGameObject.GetComponent<AIController>().tree != null) //the selected AI has a AIController Component attached.
                        {
                            currentSelectedNode = null; //set the current selected node reference null
                            LoadTree(Selection.activeGameObject.GetComponent<AIController>().tree, this); //load the AI's tree.
                        }
                    }
                }             
            }
            else
            {
                if (playMode)
                {
                    if (tree == null && selectedAgent != null)
                    {
                        LoadTree(selectedAgent.GetComponent<AIController>().tree, this); //load the prefab. (AI's tree gets reset to the original asset when leaving playmode)
                    }
                    selectedAgent = null; //no agent selected anymor 

                    playMode = false; //no longer in playmode.
                }
                
            }

            //Begin the drawing of pop up windows
            BeginWindows();
            //draw the inspector of the node data and anchor it to the right hand side of the screen.
            inspectorWindow = GUI.Window(100000000, new Rect(position.width - 300, 0, 300, position.height), DrawInspector, inspectorName);

            parameterWindow = GUI.Window(200000000, new Rect(0 + 200, 0, position.width - 500, 200), DrawParameterInspector, "Parameters");

            if (!parameterWindow.Contains(e.mousePosition) && !inspectorWindow.Contains(e.mousePosition))
            {
                if (e.type == EventType.ContextClick && !startConnection)
                {
                    //open add node context menu
                    GenericMenu addNodeMenu = new GenericMenu();
                    AddItemsToMenu(addNodeMenu);
                }
            }

          
            if (tree != null)
            {
                //if there are actually nodes in the scene.
                if (tree.nodes.Count > 0)
                {
                    //loop through and draw
                    for (int i = 0; i < tree.nodes.Count; i++)
                    {
                        tree.nodes[i].Draw(e);
                        Repaint();
                        id++;
                    }
                }
                tree.UpdateParameters();
            }

            //if the left mouse button is down and node is currently selected
            if (e.button == 0 && e.type == EventType.MouseDown && currentSelectedNode != null)
            {
                if (currentSelectedNode.startConnection) //if a connection is in process, stop it
                {
                    currentSelectedNode.startConnection = false;
                }
                if (!currentSelectedNode.connectionSelected)
                {
                    currentSelectedNode = null; //unselect it
                }

            }

            EndWindows();
            //end drawing of pop up windows
            
            //Draw save button and check if it is pressed. If pressed, Save the Tree
            if (GUI.Button(new Rect(0, 0, 100, 50),"Save"))
            {
                SaveTree();
            }
        
            //Draw textfield and set the trees name to what ever is put in the text field.
            treeName = GUI.TextField(new Rect(100, 0, 100,50), treeName);

            AutoSave();


        }

        void AutoSave()
        {
#if UNITY_EDITOR
            UnityEditor.EditorUtility.SetDirty(tree); //set all edited assets dirty, to prevent loss of data.
            foreach (var node in tree.nodes)
            {
                UnityEditor.EditorUtility.SetDirty(node);
            }
#endif
        }


        void DrawParameterInspector(int id)
        {
            //Draw the Parameter inspector window.
            if (tree != null)
            {
                ParameterUtilities.DrawButtons(this);
                ParameterUtilities.DrawIntParametersWindow(this);
                ParameterUtilities.DrawFloatParametersWindow(this);
                ParameterUtilities.DrawBoolParametersWindow(this);
            }
        }

        void DrawInspector(int id)
        {
            //if the user has selected a node
            if (currentSelectedNode != null && !currentSelectedNode.connectionSelected)
            {
                inspectorName = "Node Properties";
                //Draw textfield for naming Nodes
                GUILayout.BeginHorizontal();
                GUILayout.Label("Name:");
                currentSelectedNode.name = GUILayout.TextField(currentSelectedNode.name);
                GUILayout.EndHorizontal();
                //if the selected node isnt a root node
                if (currentSelectedNode.nodeType != NodeType.Root)
                {
                    if (currentSelectedNode.nodeType != NodeType.Selector)
                    {
                        InspectorUtilities.BehaviourPopUpMenu(this); //draw the pop up menu of all the available behaviours that can be assigned.
                    }
                    InspectorUtilities.DrawFields(this); //draw the behaviours fields.
                }              
            }
            if (currentSelectedNode != null && currentSelectedNode.connectionSelected) //if a connection curve has been selected
            {
                inspectorName = "Node Parameters";

                GUI.Label(new Rect(125, 25, 150, 20), "Add/Remove Parameter");
                if (GUI.Button(new Rect(10,25,50,20),"+")) //draw add button
                {
                    if (currentSelectedNode.localParamsDisplay.Count < 16) //if the parameter count is below threshold
                    {
                        ParameterUtilities.AddParameterToNode(currentSelectedNode); //assign parameter to node.

                        currentSelectedNode.selectedParameter.Add(0);
                    }
                    else
                    {
                        EditorUtility.DisplayDialog("Error", "Parameter Limit Reached. Please remove some before adding.", "Continue"); //Display error when cap hit
                    }
                }
                if (GUI.Button(new Rect(70, 25, 50, 20), "-")) //draw remove button
                {
                    if (currentSelectedNode.localParamsDisplay.Count > 0) //if there are parameters
                    {
                        ParameterUtilities.RemoveParameterFromNode(currentSelectedNode); //remove parameter from node.
                    }
                }

                GUI.Box(new Rect(10, 60, 280, 400), "Parameters");
                Event e = Event.current;
                //draw parameters
                for (int i = 0; i < currentSelectedNode.localParams.Count; i++)
                {
                    if (currentSelectedNode.localParamsDisplay[i] == currentSelectedNode.currentSelectedParamDisplay)
                    {
                        GUI.color = Color.gray;
                    }
                    else
                    {
                        GUI.color = Color.white;
                    }
                    GUI.Box(currentSelectedNode.localParamsDisplay[i], "");
                    if (e.button == 0 && e.type == EventType.MouseUp) //if left mouse is pressed
                    {
                        if (currentSelectedNode.localParamsDisplay[i].Contains(e.mousePosition)) //if its within a parameter display box
                        {
                            currentSelectedNode.currentSelectedParamDisplay = currentSelectedNode.localParamsDisplay[i]; //select parameter
                            currentSelectedNode.currentSelectedParam = currentSelectedNode.localParams[i]; 
                        }
                    }
                    string[] globalParams = GetAllParameterNames().ToArray();
                    Rect popUpDisplay = new Rect(currentSelectedNode.localParamsDisplay[i].x + 5, currentSelectedNode.localParamsDisplay[i].y + 5, 50, 20); //Draw all available parameters
                    currentSelectedNode.selectedParameter[i] = EditorGUI.Popup(popUpDisplay, "", currentSelectedNode.selectedParameter[i], globalParams); //set the current selected parameter

                    if (currentSelectedNode.selectedParameter[i] != 0)
                    {
                        //Draw correct UI based on the selected parameters value type
                        if (currentSelectedNode.localParams[i].type == ParameterType.INT)
                        {
                            EditorGUI.IntField(new Rect(popUpDisplay.x + 60, popUpDisplay.y - 2, 50, 18), currentSelectedNode.localParams[i].iContinuousValue);
                        }
                        if (currentSelectedNode.localParams[i].type == ParameterType.FLOAT)
                        {
                            EditorGUI.FloatField(new Rect(popUpDisplay.x + 60, popUpDisplay.y - 2, 50, 18), currentSelectedNode.localParams[i].fContinuousValue);
                        }
                        if (currentSelectedNode.localParams[i].type == ParameterType.BOOL)
                        {
                            EditorGUI.Toggle(new Rect(popUpDisplay.x + 60, popUpDisplay.y - 2, 50, 18), currentSelectedNode.localParams[i].bContinuousValue);
                        }
                    }

                    if (currentSelectedNode.selectedParameter[i] != 0 && currentSelectedNode.currentSelectedParam != null)
                    {
                        if (globalParams[currentSelectedNode.selectedParameter[i]] != "Untitled") //if a parameter was selected
                        {
                           AssignParameter(globalParams[currentSelectedNode.selectedParameter[i]], currentSelectedNode.localParams[i]); //Assign values
                        }
                    }
                }
            }
        }

        void AssignParameter(string name, Parameters param)
        {
            foreach (var i in tree.globalParameters)
            {
                if (i.paramName == name)
                {
                    param.type = i.type;
                    if (param.type == ParameterType.INT)
                    {
                        param.iConditionvalue = i.iConditionvalue;
                        param.nConditionTypes = i.nConditionTypes;
                    }
                    if (param.type == ParameterType.FLOAT)
                    {
                        param.fConditionvalue = i.fConditionvalue;
                        param.nConditionTypes = i.nConditionTypes;
                    }
                    if (param.type == ParameterType.BOOL)
                    {
                        param.bConditionvalue = i.bConditionvalue;
                        param.bConditionTypes = i.bConditionTypes;
                    }
                    param.paramName = i.paramName;
                }
            }
        }

        List<string> GetAllParameterNames()
        {
            //loop through and return all parameter names.
            List<string> paramNames = new List<string>();
            paramNames.Add("None");

            foreach (var p in tree.globalParameters)
            {
                paramNames.Add(p.paramName);
            }
            return paramNames;
        }

        void SaveTree()
        {
            if (treeName == "")
            {
                EditorUtility.DisplayDialog("Name Is required","A name is required for saving trees", "Ok");
                return;
            }

            //if the user has already saved tree, display error message and exit function.
            if (AssetDatabase.Contains(tree))
            {
                EditorUtility.DisplayDialog("Error Saving", "This Tree Has Already Been Saved, Saving Only Requred Once!!", "Ok");
                return;
            }

            string directory = "Assets/AI Behaviour"; 

            //if directory doesnt exist, create it
            if (!Directory.Exists(directory))
            {
                AssetDatabase.CreateFolder("Assets", "AI Behaviour");
            }
            //create scriptable object instance
            //set the save path and type
            string filePath = directory + "/" + treeName + ".asset";

            //create asset
            AssetDatabase.CreateAsset(tree, filePath);

            //create node assets and attach it to the tree asset.
            foreach (var n in tree.nodes)
            {
                AssetDatabase.AddObjectToAsset(n, tree);
                if (n.behaviour != null)
                {
                    AssetDatabase.AddObjectToAsset(n.behaviour, tree);
                }
            }

            tree = null;

            tree = (AITree)AssetDatabase.LoadAssetAtPath(filePath, typeof(AITree));
        }
        public void UpdateConnectionCurve(Rect start, Rect end, NodeData selectNode = null,List<NodeData> nodes = null)
        {
            TreeEditorUtilities.DrawConnectionCurves(start, end, this,selectNode, nodes);
            Repaint();
        }

       
    }
}

#endif



